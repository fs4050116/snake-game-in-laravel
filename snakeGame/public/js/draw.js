const canvas = document.querySelector(".canvas");
var ctx = canvas.getContext('2d');
const scale = 10;
const canvasheight =600;
const canvaswidth  = 800;
const rows = canvasheight / scale;
const columns = canvaswidth / scale;
var snake;

(function setup() {
  snake = new Snake();
  fruit = new Fruit();
  fruit.pickLocation();

  window.setInterval(() => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    fruit.draw();
    snake.update();
    snake.draw();

    if (snake.eat(fruit)) {
      fruit.pickLocation();
    }

    snake.checkCollision();
    document.querySelector('.score')
      .innerText = snake.total;

  }, 250);
}());

window.addEventListener('keydown', ((evt) => {
  const direction = evt.key.replace('Arrow', '');
  snake.changeDirection(direction);
}));
